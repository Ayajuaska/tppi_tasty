import logging
import os
import sys
from socket import AF_INET

from pyroute2 import IPRoute

from tasty.test_runner import TestRunner

logging.basicConfig(filename='%s.log' % __package__, level=logging.DEBUG,
                    format='%(asctime)s:%(name)s:%(levelname)s - %(message)s')

PID_FILE = '%s.pid' % __package__
logger = logging.getLogger(__package__)


def find_host():
    ipr = IPRoute()
    gws = ipr.get_default_routes(AF_INET)
    host_addr = None
    for gw in gws:
        for attr in gw['attrs']:
            if attr[0] == 'RTA_GATEWAY':
                host_addr = attr[1]
                break
    if host_addr is None:
        raise RuntimeError('Could not find host address')
    return host_addr


def main():
    if len(sys.argv) < 2:
        print('USAGE: %s <test_name>' % __package__)
        return
    test_runner = TestRunner()
    host_address = find_host()
    test_runner.set_host_addr(host_address)

    if os.path.exists(PID_FILE):
        test_runner.enqueue(sys.argv[1])
    else:
        try:
            pid = os.fork()
            if pid > 0:
                print(pid)
                exit(0)
        except OSError as err:
            sys.stderr.write('fork #2 failed: {0}\n'.format(err))
            sys.exit(1)

        os.setsid()
        # os.umask(0)
        sys.stdout.flush()
        sys.stderr.flush()
        si = open(os.devnull, 'r')
        se = open(os.devnull, 'a+')

        os.dup2(si.fileno(), sys.stdin.fileno())
        os.dup2(se.fileno(), sys.stderr.fileno())

        pid = str(os.getpid())
        with open(PID_FILE, 'w+') as f:
            f.write('%s\n' % pid)
        logger.debug('Starting daemon')
        test_runner.enqueue(sys.argv[1], True)
        test_runner.run()
        os.remove(PID_FILE)


if __name__ == '__main__':
    main()
