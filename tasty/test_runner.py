import io
import os
import sys
import threading
from dataclasses import dataclass
from logging import getLogger
from queue import Queue

import pytest
import zmq

from tasty import config

TASK_LISTENER_SOCK = 'tcp://127.0.0.1:9999'


@dataclass
class TestResult:
    name: str
    res: int
    std_out: str
    std_err: str

    def __str__(self):
        return "%s's Result: %s\nOutput:\n%s\nErrors:\n%s\n" % (self.name, self.res, self.std_out, self.std_err)


class TestRunner:

    host_address: str
    task_listener_thread: threading.Thread

    def __init__(self):
        self.logger = getLogger(self.__class__.__name__)
        self.config = config
        self.test_queue = Queue()
        self.test_set = set()
        self.running = False

        with open(config.TEST_SET, 'r') as set_src:
            self.test_set = set(set_src.read().split())
            self.logger.debug('Test set: %s', self.test_set)

    def enqueue(self, name, immediate=False):
        self.logger.debug('Enqueue %s', name)
        test = name.split(':')[0]
        if test not in self.test_set:
            self.fail(name, 'No such test in test set: %s' % test)
            return

        if immediate:
            self.test_queue.put_nowait(name)
        else:
            context = zmq.Context()
            socket = context.socket(zmq.REQ)
            socket.connect(TASK_LISTENER_SOCK)
            socket.send_string(name)
            socket.close()

    def task_listener(self):
        context = zmq.Context()
        socket = context.socket(zmq.REP)
        socket.RCVTIMEO = 500
        socket.bind(TASK_LISTENER_SOCK)
        self.logger.debug('Listening %s', TASK_LISTENER_SOCK)
        while self.running:
            try:
                name = socket.recv_string()
                self.logger.debug('Received: %s', name)
                # name = name.decode()
                self.logger.debug('Received: %s', name)
            except Exception:
                continue
            test = name.split(':')[0]
            if test in self.test_set:
                self.test_queue.put(name)
            else:
                self.fail(name, 'No such test in test set: %s' % test)

    def update_status(self, test_result: TestResult):
        status_message = {
            'name': test_result.name,
            'result': test_result.res,
            'output': test_result.std_out,
            'errors': test_result.std_err,
        }
        self.logger.debug('Result: %s', test_result)
        self.logger.debug('Sending result to %s', self.host_address)
        context = zmq.Context()
        socket = context.socket(zmq.REQ)
        socket.setsockopt(zmq.LINGER, 5)
        socket.disable_monitor()
        socket.SNDTIMEO = 500
        socket.connect('tcp://%s:8888' % self.host_address)
        socket.send(('%s' % status_message).encode(), track=False)
        socket.close()

    def fail(self, name, reason):
        self.update_status(
            TestResult(name, 1, '', reason)
        )

    def run(self):
        self.logger.debug('Queue is %s', 'empty' if self.test_queue.empty() else 'not empty')
        self.running = True
        self.task_listener_thread = threading.Thread(target=self.task_listener)
        self.task_listener_thread.start()
        while not self.test_queue.empty():
            task = self.test_queue.get()
            self.logger.debug('Running task %s', task)
            self.test_queue.task_done()
            if config.TEST_TYPE == 'python':
                result = self.run_py_test(task)
            else:
                result = self.run_bash_test(task)
            self.update_status(result)
        self.running = False
        self.task_listener_thread.join(timeout=1)

    def run_py_test(self, test):
        self.logger.debug('Running python test: %s', test)
        __tst_io = {
            'out': io.StringIO(''),
            'err': io.StringIO(''),
        }

        pytest_args = ['-v', '--capture=no', '--rootdir=%s' % config.SUBJECT_DIR, os.path.join(config.TESTS_DIR, test)]

        old_io = sys.stdout, sys.stderr
        sys.stdout, sys.stderr = __tst_io['out'], __tst_io['err']
        res = pytest.main(pytest_args)
        sys.stdout, sys.stderr = old_io

        return TestResult(
            test,
            res,
            __tst_io['out'].getvalue(),
            __tst_io['err'].getvalue(),
        )

    def run_bash_test(self, test):
        return TestResult(
            test,
            0,
            '',
            '',
        )

    def set_host_addr(self, host_address):
        self.host_address = host_address
